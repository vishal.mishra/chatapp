var channel_id = 0;
var currentChannelId;
var username;

// retrieves the channel-list from Database at startup or refresh of page

$(document).ready( makeGetRequest("http://0.0.0.0:5000/channels/", function(data){
            
    let dbdata = data.resources
    $.each(dbdata, function(index, value){
        $('div.chatroom-tab').append($('<p>').attr('channel_id', value.channel_id).text('@'+value.Channel));
        channel_id = value.channel_id;
        })
})
)

// make a POST function

function makePost(url, data, successFunction){
    $.ajax({
        type: 'POST',
        url: url,
        data: JSON.stringify(data),
        dataType:'json',
        contentType: 'application/json',  
        success: successFunction
    })
}


// make a GET request

function makeGetRequest(url, successFunction){
    $.ajax({
        type: 'GET',
        url: url,
        success: successFunction
    })
}


// send messages the database and show on screen

$('.send-btn').click(function(e){
    let messagedata = $('#message').val();
    makePost('http://0.0.0.0:5000/messages/', { message: messagedata, channel_id: currentChannelId, username: username}, function(){
        
    })
    makePost('http://0.0.0.0:5000/broadcast', {message: messagedata, channel_id: currentChannelId, username: username}, function(){

    })
})

// Toggle add-channel effect

$(".add-channel-link").click(function(){
    $(this).hide()
    $('div.add-channel').fadeIn()

})

// add channels to the channel list area and post it in database using sandman server

$('.add-channel-btn').click(function(){
    $('div.add-channel').hide();
    $('p.add-channel-link').fadeIn();

    let channel_name = $('#add-channel').val();
    makePost('http://0.0.0.0:5000/channels/', {Channel: channel_name}, function(){
        channel_id++;
        //let p = $('<p channel_id='+ channel_id +'>@'+ channel_name + '</p>')
        $('div.chatroom-tab').append($('<p>').attr('channel_id', channel_id).text('@'+channel_name));
        })
})

// send the message in db
$('.channel').click(function(e){
    currentChannelId = $(e.target).attr('channel_id');
    $('.highlight-channel').removeClass('highlight-channel')
    $(e.target).addClass("highlight-channel")
    $('.message-area').empty();
    retrieveMessages();
})

function retrieveMessages(){
    makeGetRequest('http://0.0.0.0:5000/messages/', function(data){
        //console.log(data.resources)
        $.each(data.resources, function(index, value){
            //console.log(value.channel_id + value.message)
            if (value.channel_id === Number(currentChannelId)){
               $('.message-area').append($('<p>').text(value.username + " :").css("font-weight", "bold"));
        $('.message-area').append($('<p>').text(value.message).css("font-size","14px").css("padding","0 3%"));
        $('.message-area').append($('<hr>').addClass("hrline"))
            }
            })
        })
}

function Login(showhide) {
    if (showhide == "show") {
        $("#popupbox").removeClass('hidden');
        }
    else if (showhide == "hide") {
        $("#popupbox").addClass('hidden');
        username = $('#username').val();
        $('div.login').empty()
        $('div.login').text('Welcome, ' + username);
        $('.login').attr("style","margin: 0")
    }
 }
